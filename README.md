# How to properly flash OpenWrt onto ASUS routers.
#### Tested with RT-AC750.
### **If you're using the RT-AC750 like me, use the RT-AC51U firmware, they're the exact same.**

### **Disclaimer: I am not responsible for bricking your device, the information below worked for me, and will probably work for you.**
I've recently flashed OpenWrt onto my RT-AC750 and had to
spend a long time understanding how to actually flash it.
I decided to create this repository to help myself/others
in the future with the flashing process.


#### Prequisites:
* A computer or VM running Windows 10
* A working CAT5e/CAT6 cable
* A working ASUS router

#### Downloading the OpenWrt firmware
First, (assuming you didn't already) go to [OpenWrt's firmware download page](https://openwrt.org/toh/views/toh_fwdownload?dataflt%5BBrand*~%5D=ASUS)
and find your device. In this guide I'll use the device I flashed, the RT-AC5750.
After finding your device page, look for "Firmware OpenWrt Upgrade" and download the file ending on `sysupgrade.bin`

#### Downloading the ASUS Restoration Tool
Once you've downloaded the OpenWrt firmware
1. Go to [ASUS's website](https://www.asus.com/)
2. Click the magnifying glass and search your router model

On the router's page, click "Support" (next to "Review")
1. On the "Support" page, select "Driver & Utility"
2. Choose your OS (preferrably Win10 64-bit)
3. In the "Software and Utility" part, find "ASUS Firmware Restoration version" and download/unzip it
    - You may have to click "Show all"
4. Run the tool and install the program
  	- The program is stored in `C:\Program Files (x86)\ASUS\Wireless Router\Firmware Restoration` by default

#### Flashing OpenWrt onto your router
After you've downloaded both things above, it's time to actually flash OpenWrt.

**Optional**: Clear the NVRAM (router configuration)
- Plug the power cable out, then hold the WPS button
- While holding the WPS button, plug the cable in again and power the router on
- Hold the WPS button for 30sec MAX
- Your router should be blinking fast as long as you hold WPS
- The NVRAM was cleared.

**Flashing OpenWrt**

**You should disable any other network interfaces just in case**
1. Put your router into recovery mode
    - Plug the power cable out
    - Hold the reset button with a pin
    - While holding reset, plug the cable in
        - While in recovery mode, your router should blink slowly.

2. Plug in a RJ45 cable into LAN1 slot on your router
3. Open the ASUS Restoration Tool
    - Select the OpenWrt firmware
    - Click "Upload"
    - Note: Some sites like OpenWrt wiki / YouTube say you should set a static IP.  
      For me it worked without that, maybe because I'm using a Win10 VM with a [TAP interface](https://gist.github.com/extremecoders-re/e8fd8a67a515fee0c873dcafc81d811c#setup).

#### **Congratulations, you've successfully flashed OpenWrt!**